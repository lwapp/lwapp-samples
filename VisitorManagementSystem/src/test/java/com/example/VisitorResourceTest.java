package com.example;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.junit.Assert;
import org.junit.Test;
import org.lwapp.hibernate.persistence.util.TransactionHelper;

import com.example.persistence.dao.VisitorDao;
import com.example.persistence.entity.VisitorEntity;
import com.example.rest.domain.VisitorRegistrationRequest;
import com.example.rest.resources.VisitorResource;

public class VisitorResourceTest extends BasicTest {

    @Inject
    private VisitorDao visitorDao;
    @Inject
    private VisitorResource visitorResource;
    @Inject
    private TransactionHelper TransactionHelper;

    @Test
    public void testPersistence() throws Exception {
        final VisitorEntity findById = TransactionHelper.executeInTransaction(() -> {
            final VisitorEntity entity = new VisitorEntity();
            entity.setId(123L);
            entity.setEmployeeName("Tom");
            entity.setPurpose("Friendship");
            entity.setVisitorName("Merry");
            visitorDao.persist(entity);
            return visitorDao.findById(entity.getId());
        });
        Assert.assertNotNull(findById);
    }

    @Test
    public void testRegistration() throws ParseException {
        final VisitorRegistrationRequest request = new VisitorRegistrationRequest();
        request.setEmployeeName("Tom");
        request.setPurpose("Friendship");
        request.setVisitorName("Merry");
        Response response = visitorResource.registerVistor(request);
        Assert.assertEquals("Visitor registerd successfully", response.getEntity().toString());

        response = visitorResource.getVisitorsByDate(DateFormatUtils.format(new Date(), "yyyyMMdd"));
        Assert.assertEquals(200, response.getStatus());
        @SuppressWarnings("unchecked")
        final List<VisitorEntity> list = (List<VisitorEntity>) response.getEntity();
        Assert.assertEquals(2, list.size());

    }

}
