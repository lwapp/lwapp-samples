package com.example;

import javax.inject.Inject;

import org.hibernate.cfg.Configuration;
//import org.jglue.cdiunit.CdiRunner;
import org.junit.Before;
import org.junit.BeforeClass;
//import org.junit.runner.RunWith;
import org.lwapp.hibernate.persistence.util.HibernateUtils;

//@RunWith(CdiRunner.class)
public abstract class BasicTest {
    @Inject
    HibernateUtils HibernateUtils;

    @BeforeClass
    public static void beforeClass() throws Exception {
        //        final Configuration hibernateConfiguration = new Configuration().configure("hibernate-test.cfg.xml");

    }

    @Before
    public void before() {
        final Configuration hibernateConfiguration = new Configuration().configure("hibernate-test.cfg.xml");
        HibernateUtils.buildSessionFactory(hibernateConfiguration, "com.example");
        //        configClient.loadConfigurations(IoTools.getResourceUrl("application.test.properties"));
        //        Assert.assertEquals("3131", configClient.getString(LwappConfigurations.HTTP_PORT));
    }

}
