package com.example.rest.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotBlank;

@XmlRootElement
public class VisitorRegistrationRequest implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @NotBlank(message = "VisitorName is mandatory")
    private String visitorName;
    @NotBlank(message = "Employee name is mandatory.")
    private String employeeName;
    @NotBlank(message = "Purpose of visit is mandatory")
    private String purpose;

    public String getVisitorName() {
        return visitorName;
    }

    public void setVisitorName(final String visitorName) {
        this.visitorName = visitorName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(final String employeeName) {
        this.employeeName = employeeName;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(final String purpose) {
        this.purpose = purpose;
    }

}
