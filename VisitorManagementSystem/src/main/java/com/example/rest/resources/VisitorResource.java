package com.example.rest.resources;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.time.DateUtils;
import org.lwapp.hibernate.interceptor.UnitOfWork;
import org.lwapp.hibernate.interceptor.UnitOfWorkInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.persistence.dao.VisitorDao;
import com.example.persistence.entity.VisitorEntity;
import com.example.rest.domain.VisitorRegistrationRequest;

@Path("visitors")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Interceptors(UnitOfWorkInterceptor.class)
public class VisitorResource {
    private static final Logger LOG = LoggerFactory.getLogger(VisitorResource.class);

    @Inject
    private VisitorDao visitorDao;

    @POST
    @UnitOfWork
    public Response registerVistor(final VisitorRegistrationRequest request) {

        final VisitorEntity entity = new VisitorEntity();
        entity.setId(visitorDao.getUniqueId());
        entity.setEmployeeName(request.getEmployeeName());
        entity.setPurpose(request.getPurpose());
        entity.setVisitorName(request.getVisitorName());
        visitorDao.persist(entity);

        return Response.ok("Visitor registerd successfully").build();
    }

    // List all Visitors by given date (yyyyMMdd)
    @GET
    @UnitOfWork
    public Response getVisitorsByDate(@QueryParam("date") final String yyyyMMdd) throws ParseException {
        final Date date = DateUtils.parseDateStrictly(yyyyMMdd, "yyyyMMdd");
        LOG.info("Searching by date:{}", date);
        final List<VisitorEntity> visitors = visitorDao.findVisitorsByDate(date);
        return Response.ok(visitors).build();
    }

}
