package com.example;

import javax.inject.Inject;

import org.lwapp.configclient.ApplicationPropertiesPersister;
import org.lwapp.configclient.client.ConfigurationServiceClient;
import org.lwapp.core.AbstractApplicationMain;

public class ApplicationMain extends AbstractApplicationMain {

    @Inject
    private ApplicationPropertiesPersister applicationConfigurationManager;
    @Inject
    private ConfigurationServiceClient configurationServiceClient;

    @Override
    protected String getUrlContext() {
        return "vms";
    }

    @Override
    protected void initApplication() {
        configurationServiceClient.registerApplicationPropertiesPersiter(applicationConfigurationManager);
    }

}
