package com.example;

import org.lwapp.configclient.Configurable;
import org.lwapp.configclient.Configuration;

public enum VmsConfigurations implements Configurable {

    VMS_SYSTEM_EMAIL("Please provide the system email id?");

    Configuration conf;

    private VmsConfigurations(final String description) {
        conf = getConfiguration(name(), null, description, false, true);
    }

    @Override
    public Configuration getConfiguration() {
        return conf;
    }

}
