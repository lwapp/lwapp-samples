package com.example.persistence.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotBlank;
import org.lwapp.hibernate.persistence.common.AbstractEntity;

@XmlRootElement
@Entity
public class VisitorEntity extends AbstractEntity {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    private Long id;
    @NotBlank(message = "VisitorName is mandatory")
    private String visitorName;
    @NotBlank(message = "Employee name is mandatory.")
    private String employeeName;
    private String purpose;
    @Temporal(TemporalType.TIMESTAMP)
    private Date visitingTime = new Date();

    @Override
    public Long getId() {
        return id;
    }

    public String getVisitorName() {
        return visitorName;
    }

    public void setVisitorName(final String visitorName) {
        this.visitorName = visitorName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(final String employeeName) {
        this.employeeName = employeeName;
    }

    public Date getVisitingTime() {
        return visitingTime;
    }

    public void setVisitingTime(final Date visitingTime) {
        this.visitingTime = visitingTime;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(final String purpose) {
        this.purpose = purpose;
    }

}
