package com.example.persistence.dao;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.time.DateUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.lwapp.hibernate.persistence.common.AbstractEntityDao;

import com.example.persistence.entity.VisitorEntity;

public class VisitorDao extends AbstractEntityDao<VisitorEntity> {

    @Override
    public VisitorEntity persist(final VisitorEntity entity) {
        return super.persist(entity);
    }

    public List<VisitorEntity> findVisitorsByDate(final Date date) {
        Validate.notNull(date, "Date is mandatory in the search criteria");
        final Criteria criteria = criteria()//
                .add(Restrictions.ge("visitingTime", new java.sql.Date(date.getTime())))
                .add(Restrictions.le("visitingTime", DateUtils.addDays(date, 1)));
        return list(criteria);
    }

    public Long getUniqueId() {
        return RandomUtils.nextLong(10000, 999999) + System.nanoTime();
    }

}
