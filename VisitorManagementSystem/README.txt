Weld is used in the core part of the application.
And
'org.jglue.cdi-unit:cdi-unit:3.1.3' is used in the Junit dependency.

Due to this you wont able to run the application from eclipse, however Junits should work fine.
If you want to run/execute the application then the preferred way is to do "gradlew clean build distZip" and then extract the zip and execute the .bat or .sh file.

