package com.example;

import org.lwapp.jms.common.JmsDestination;
import org.lwapp.jms.common.utils.OpenMqTools;

public class JmsMessageSender {

    // Please make sure your JMS broker is up and running while testing.
    public static void main(final String... strings) throws Exception {
        //        while (true) {
        final String jmsMessage = String.valueOf(System.nanoTime());
        OpenMqTools.sendJmsMessage(new JmsDestination("localhost:7676", "VisitorQueue", "VisitorQueue"), jmsMessage);
        System.out.println(jmsMessage + " Message is sent.");
        //        }
    }

}
