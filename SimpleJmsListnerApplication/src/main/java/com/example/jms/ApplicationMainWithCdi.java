package com.example.jms;

import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.jboss.weld.environment.se.StartMain;
import org.jboss.weld.environment.se.events.ContainerInitialized;
import org.lwapp.jms.config.LwappConfigurations;

// Please make sure your JMS broker is up and running
public class ApplicationMainWithCdi {

    // Even though we don't use the below field, we should declare in order to execute the @postConstruct in the AbstractJmsQueuePoller
    @SuppressWarnings("unused")
    @Inject
    private VisitorRegistrationJmsPoller visitorRegistrationJmsPoller;

    public static void main(final String... args) {
        StartMain.main(args);
    }

    public final void start(@Observes final ContainerInitialized event) throws Exception {
        try {
            System.out.println("JMS Listner application started...");
        } catch (final Exception e) {
            e.printStackTrace();
            System.err.println("Please make sure the JMS broker is running as per the application.properties file for the property:"
                    + LwappConfigurations.JMS_URL.getConfiguration().getPropertyName());
        }
    }

}
