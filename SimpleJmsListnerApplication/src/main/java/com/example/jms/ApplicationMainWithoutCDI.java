package com.example.jms;

import org.lwapp.jms.config.LwappConfigurations;

// Please make sure your JMS broker is up and running
public class ApplicationMainWithoutCDI {

    private static final VisitorRegistrationJmsPoller visitorRegistrationJmsPoller = new VisitorRegistrationJmsPoller();

    public static void main(final String... args) {
        try {
            visitorRegistrationJmsPoller.init();
            System.out.println("JMS Listner application started...");
        } catch (final Exception e) {
            e.printStackTrace();
            System.err.println("Please make sure the JMS broker is running as per the application.properties file for the property:"
                    + LwappConfigurations.JMS_URL.getConfiguration().getPropertyName());
        }
    }

}
