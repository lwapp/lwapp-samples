package com.example.jms;

import javax.inject.Singleton;

import org.lwapp.jms.common.JmsDestination;
import org.lwapp.jms.common.incoming.AbstractJmsQueuePoller;

@Singleton
public class VisitorRegistrationJmsPoller extends AbstractJmsQueuePoller<String> {

    @Override
    protected JmsDestination getInJmsDestination() {
        return new JmsDestination("localhost:7676", "VisitorQueue", "VisitorQueue");
    }

    @Override
    protected JmsDestination getErrorQueueJmsDestination() {
        return new JmsDestination("localhost:7676", "VisitorErrorQueue", "VisitorErrorQueue");
    }

    @Override
    protected void afterReadingMessage(final String jmsObject) throws Exception {
        System.out.println(jmsObject);
    }

}
